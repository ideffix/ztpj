package jdbc.model;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "traders")
public class TraderList {
    private List<Trader> traders;

    @XmlElement(name = "trader")
    public List<Trader> getTraders() {
        return traders;
    }
}
