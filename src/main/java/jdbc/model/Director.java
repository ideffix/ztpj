package jdbc.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
@XmlType(name = "director")
public class Director extends Employee implements Serializable {
    private BigDecimal businessAllowance;
    private String cardNumber;
    private BigDecimal costLimitPerMonth;

    public Director(String pesel,
                    String name,
                    String surname,
                    String position,
                    BigDecimal salary,
                    String phoneNumber,
                    BigDecimal businessAllowance,
                    String cardNumber,
                    BigDecimal costLimitPerMonth) {
        super(pesel, name, surname, position, salary, phoneNumber);
        this.businessAllowance = businessAllowance;
        this.cardNumber = cardNumber;
        this.costLimitPerMonth = costLimitPerMonth;
    }
}
