package jdbc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlType(name = "employee")
public class Employee implements Serializable {
    private int id;
    private String pesel;
    private String name;
    private String surname;
    private String position;
    private BigDecimal salary;
    private String phoneNumber;

    public Employee(String pesel, String name, String surname, String position, BigDecimal salary, String phoneNumber) {
        this.pesel = pesel;
        this.name = name;
        this.surname = surname;
        this.position = position;
        this.salary = salary;
        this.phoneNumber = phoneNumber;
    }
}
