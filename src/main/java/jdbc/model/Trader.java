package jdbc.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
@XmlType(name = "trader")
public class Trader extends Employee implements Serializable {
    private BigDecimal comissionRate;
    private BigDecimal comissionRateLimitPerMonth;

    public Trader(String pesel,
                  String name,
                  String surname,
                  String position,
                  BigDecimal salary,
                  String phoneNumber,
                  BigDecimal comissionRate,
                  BigDecimal comissionRateLimitPerMonth) {
        super(pesel, name, surname, position, salary, phoneNumber);
        this.comissionRate = comissionRate;
        this.comissionRateLimitPerMonth = comissionRateLimitPerMonth;
    }
}
