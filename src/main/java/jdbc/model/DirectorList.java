package jdbc.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "directors")
public class DirectorList {
    private List<Director> directors;

    @XmlElement(name = "director")
    public List<Director> getDirectors() {
        return directors;
    }
}
