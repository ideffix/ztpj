package jdbc.provider;

import java.util.Scanner;

public class UserInputProvider {

    private static final Scanner reader = new Scanner(System.in);

    public static String getUserInput() {
        return reader.next();
    }

    public static String getHiddenUserInput() {
        return reader.next();
    }
}
