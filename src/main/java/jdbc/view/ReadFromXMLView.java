package jdbc.view;

import jaxb.JAXBUtils;
import jdbc.model.DirectorList;
import jdbc.model.TraderList;

import javax.xml.bind.JAXBException;
import java.io.File;

import static jdbc.utils.PrintUtils.printAndGetUserInput;

public class ReadFromXMLView implements Showable {
    private static final String PATH = "src/main/resources/";

    @Override
    public void show() {
        showHeader();
        String directorsFileName = printAndGetUserInput("Podaj nazwę pliku dyrektorów:");
        String tradersFileName = printAndGetUserInput("Podaj nazwę pliku handlarzy:");
        try {
            DirectorList directorList = JAXBUtils.readFromFile(new File(PATH + directorsFileName), DirectorList.class);
            TraderList traderList = JAXBUtils.readFromFile(new File(PATH + tradersFileName), TraderList.class);
            directorList.getDirectors().forEach(System.out::println);
            traderList.getTraders().forEach(System.out::println);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    private void showHeader() {
        System.out.println("\t6. Wyświetl dane z XML");
    }
}
