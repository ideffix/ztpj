package jdbc.view;

import jdbc.dao.DirectorDao;
import jdbc.dao.TraderDao;
import jdbc.model.Director;
import jdbc.model.Employee;
import jdbc.model.Trader;
import jdbc.utils.Action;
import networking.client.Client;
import rmi.validator.Validator;
import rmi.validator.impl.RemoteValidatorImpl;

import java.rmi.RemoteException;
import java.util.List;

import static java.lang.String.format;
import static jdbc.utils.PrintUtils.printAndGetUserInput;

public class NetworkView implements Showable {
    private DirectorDao directorDao = new DirectorDao();
    private TraderDao traderDao = new TraderDao();
    private Validator validator = new RemoteValidatorImpl();
    private Client client;

    @Override
    public void show() {
        showHeader();
        String username = printAndGetUserInput("Podaj użytkownika:");
        String password = printAndGetUserInput("Podaj hasło:");
        String address = printAndGetUserInput("Adres:");
        int port = Integer.valueOf(printAndGetUserInput("Port:"));
        try {
            String token = getToken(username, password);
            List<Employee> employees = getEmployees(address, port, token);
            System.out.println(format("Znaleziono %s wyników w sieci", employees.size()));
            handleUserDecision(
                    printAndGetUserInput("Czy zapisać pobrane dane? [T]/[N]"),
                    () -> saveToDatabase(employees));
        } catch (RemoteException e) {
            System.err.println("Nieprawidłowe dane logowania");
        }
    }

    private String getToken(String username, String password) throws RemoteException {
        return validator.authenticate(username, password);
    }

    private void saveToDatabase(List<Employee> employees) {
        employees.forEach(e -> {
            if (e instanceof Director) {
                directorDao.save((Director) e);
            } else {
                traderDao.save((Trader) e);
            }
        });
    }

    private void handleUserDecision(String userInput, Action onSave) {
        switch (userInput) {
            case "T":
                onSave.invoke();
                break;
            case "N":
                break;
            default:
                System.err.println("Wybrano niewłaściwą opcję");
                break;
        }
    }

    private List<Employee> getEmployees(String address, int port, String token) {
        client = new Client(address, port);
        return client.getEmployees(token);
    }

    private void showHeader() {
        System.out.println("\t5. Pobierz dane z sieci");
    }
}
