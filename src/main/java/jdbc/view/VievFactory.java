package jdbc.view;

public class VievFactory {

    public static Showable get(int value) {
        switch (value) {
            case 1:
                return new EmployeeListView();
            case 2:
                return new AddEmployeeView();
            case 3:
                return new RemoveEmployeeView();
            case 4:
                return new BackupView();
            case 5:
                return new NetworkView();
            case 6:
                return new ReadFromXMLView();
            case 7:
                return new SaveToXMLView();
            default:
                return new MainView();
        }
    }
}
