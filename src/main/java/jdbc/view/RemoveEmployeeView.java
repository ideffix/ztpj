package jdbc.view;

import jdbc.dao.DirectorDao;
import jdbc.dao.TraderDao;
import jdbc.model.Director;
import jdbc.model.Trader;
import jdbc.provider.UserInputProvider;
import jdbc.utils.Action;
import jdbc.utils.PrintUtils;

import java.util.Optional;

public class RemoveEmployeeView implements Showable {
    private DirectorDao directorDao = new DirectorDao();
    private TraderDao traderDao = new TraderDao();

    @Override
    public void show() {
        showHeader();
        String pesel = UserInputProvider.getUserInput();
        Optional<Trader> optionalTrader = traderDao.getByPesel(pesel);
        if (optionalTrader.isPresent()) {
            Trader trader = optionalTrader.get();
            PrintUtils.renderTraderView(trader);
            showFooter();
            handleUserInput(
                    UserInputProvider.getUserInput(),
                    () -> traderDao.delete(trader.getId())
            );
            return;
        }
        Optional<Director> optionalDirector = directorDao.getByPesel(pesel);
        if (optionalDirector.isPresent()) {
            Director director = optionalDirector.get();
            PrintUtils.renderDirectorView(director);
            showFooter();
            handleUserInput(
                    UserInputProvider.getUserInput(),
                    () -> directorDao.delete(director.getId())
            );
            return;
        }
        handleNoResult();
    }

    private void showFooter() {
        StringBuilder sb = new StringBuilder();
        sb.append("[U] – usuń\n");
        sb.append("[Q] – porzuć\n");
        sb.append("Wybór>");
        System.out.println(sb.toString());
    }

    private void handleNoResult() {
        System.out.println("Nie znaleziono wyników dla zadanego PESELu");
    }

    private void handleUserInput(String userInput, Action onDelete) {
        switch (userInput) {
            case "U":
                onDelete.invoke();
                break;
            case "Q":
                break;
            default:
                System.err.println("Nieprawidłowy wybór");
                break;
        }
    }

    private void showHeader() {
        System.out.println("3.\tUsuń pracownika\n");
        System.out.println("\tPodaj PESEL:\n");
    }
}
