package jdbc.view;

import jaxb.JAXBUtils;
import jdbc.dao.DirectorDao;
import jdbc.dao.TraderDao;
import jdbc.model.DirectorList;
import jdbc.model.TraderList;

import javax.xml.bind.JAXBException;
import java.io.File;

import static jdbc.utils.PrintUtils.printAndGetUserInput;

public class SaveToXMLView implements Showable {
    private static final String PATH = "src/main/resources/";
    private TraderDao traderDao = new TraderDao();
    private DirectorDao directorDao = new DirectorDao();

    @Override
    public void show() {
        showHeader();
        TraderList tl = new TraderList(traderDao.getAll());
        DirectorList dl = new DirectorList(directorDao.getAll());
        String directorsFileName = printAndGetUserInput("Podaj nazwę pliku dyrektorów:");
        String tradersFileName = printAndGetUserInput("Podaj nazwę pliku handlarzy:");
        try {
            JAXBUtils.saveToFile(
                    new File(PATH + directorsFileName),
                    dl,
                    DirectorList.class);
            JAXBUtils.saveToFile(
                    new File(PATH + tradersFileName),
                    tl,
                    TraderList.class);
            System.out.println("Dane pomyślnie zapisano do plików");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    private void showHeader() {
        System.out.println("\t7. Zapisz dane z bazy danych do XML");
    }
}
