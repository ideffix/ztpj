package jdbc.view;

import jdbc.provider.UserInputProvider;
import org.apache.commons.lang3.math.NumberUtils;

public class MainView implements Showable {

    @Override
    public void show() {
        StringBuilder sb = new StringBuilder();
        sb.append("MENU\n");
        sb.append("\t1.\tLista pracowników\n");
        sb.append("\t2.\tDodaj pracownika\n");
        sb.append("\t3.\tUsuń pracownika\n");
        sb.append("\t4.\tKopia zapasowa\n");
        sb.append("\t5.\tPobierz dane z sieci\n");
        sb.append("\t6.\tWyświetl dane z XML\n");
        sb.append("\t7.\tZapisz dane z bazy danych do XML\n\n");
        sb.append("Wybór>");
        System.out.println(sb);
        handleUserInput(UserInputProvider.getUserInput());
        show();
    }

    private void handleUserInput(String userInput) {
        if (NumberUtils.isCreatable(userInput)) {
            Integer value = Integer.valueOf(userInput);
            if (value > 0 && value <= 7) {
                handleUserInput(value);
                return;
            }
        }
        System.err.println("Nieprawidłowy wybór :( Spróbuj ponownie");
        show();
    }

    private void handleUserInput(Integer value) {
        VievFactory.get(value).show();
    }
}
