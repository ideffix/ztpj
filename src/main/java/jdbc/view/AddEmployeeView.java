package jdbc.view;

import jdbc.dao.DirectorDao;
import jdbc.dao.TraderDao;
import jdbc.model.Director;
import jdbc.model.Employee;
import jdbc.model.Trader;
import jdbc.provider.UserInputProvider;
import jdbc.utils.Action;

import java.math.BigDecimal;

import static jdbc.utils.PrintUtils.printAndGetUserInput;

public class AddEmployeeView implements Showable {
    private DirectorDao directorDao = new DirectorDao();
    private TraderDao traderDao = new TraderDao();

    @Override
    public void show() {
        showHeader();
        handleUserInput(UserInputProvider.getUserInput());
    }

    private void handleUserInput(String userInput) {
        switch (userInput) {
            case "D":
                handleDirectorAdd();
                break;
            case "H":
                handleTraderAdd();
                break;
            default:
                System.err.println("Nieprawidłowy wybór, spróbuj ponownie");
                show();
                break;
        }
    }

    private void handleDirectorAdd() {
        Director director = new Director();
        getCommonData(director);
        director.setBusinessAllowance(new BigDecimal(printAndGetUserInput("Dodatek służbowy:")));
        director.setCardNumber(printAndGetUserInput("Karta służbowa numer:"));
        director.setCostLimitPerMonth(new BigDecimal(printAndGetUserInput("Limit kosztów/miesiąc")));

        showFooter();
        handleUserDecisionInput(
                UserInputProvider.getUserInput(),
                () -> directorDao.save(director)
        );
    }

    private void handleUserDecisionInput(String userInput, Action onSave) {
        switch (userInput) {
            case "Z":
                onSave.invoke();
                break;
            case "Q":
                break;
            default:
                System.err.println("Nieprawidłowy wybór");
                break;
        }
    }

    private void handleTraderAdd() {
        Trader trader = new Trader();
        getCommonData(trader);
        trader.setComissionRate(new BigDecimal(printAndGetUserInput("Prowizja:")));
        trader.setComissionRateLimitPerMonth(new BigDecimal(printAndGetUserInput("Limit prowizji/miesiąc")));

        showFooter();
        handleUserDecisionInput(
                UserInputProvider.getUserInput(),
                () -> traderDao.save(trader)
        );
    }

    private void getCommonData(Employee employee) {
        employee.setPesel(printAndGetUserInput("Identyfikator PESEL:"));
        employee.setName(printAndGetUserInput("Imię:"));
        employee.setSurname(printAndGetUserInput("Nazwisko:"));
        employee.setPosition(printAndGetUserInput("Pozycja:"));
        employee.setSalary(new BigDecimal(printAndGetUserInput("Wynagrodzenie:")));
        employee.setPhoneNumber(printAndGetUserInput("Telefon:"));
    }

    private void showHeader() {
        StringBuilder sb = new StringBuilder();
        sb.append("2.\tDodaj Pracownika\n");
        sb.append("\t[D]yrektor/[H]andlowiec:\n");
        sb.append("Wybór>");
        System.out.println(sb.toString());
    }

    private void showFooter() {
        StringBuilder sb = new StringBuilder();
        sb.append("[Z] – zapisz\n");
        sb.append("[Q] – porzuć\n");
        sb.append("Wybór>");
        System.out.println(sb.toString());
    }

}
