package jdbc.view;

import jdbc.dao.DirectorDao;
import jdbc.dao.TraderDao;
import jdbc.model.Director;
import jdbc.model.Trader;
import jdbc.provider.UserInputProvider;
import jdbc.utils.PrintUtils;

import java.util.List;

import static java.lang.String.format;

public class EmployeeListView implements Showable {
    private DirectorDao directorDao = new DirectorDao();
    private TraderDao traderDao = new TraderDao();

    private List<Director> directors;
    private List<Trader> traders;
    private int step = 1;

    public EmployeeListView() {
        getInitialData();
    }

    @Override
    public void show() {
        renderHeader();
        if (directors.isEmpty() && traders.isEmpty()) {
          System.out.println("Brak wyników");
          return;
        } else if (step <= directors.size()) {
            PrintUtils.renderDirectorView(directors.get(step - 1));
        } else {
            PrintUtils.renderTraderView(traders.get(step - directors.size() - 1));
        }
        renderFooter();
        handleUserInput(UserInputProvider.getUserInput());
    }

    private void handleUserInput(String userInput) {
        switch (userInput.toLowerCase()) {
            case "n":
                if (step < directors.size() + traders.size()) {
                    step++;
                }
                show();
                break;
            case "q":
                return;
            default:
                System.err.println("Nieprawidłowy wybór :( Spróbuj ponownie");
                show();

        }
    }

    private void renderHeader() {
        System.out.println("\t1.\tLista pracowników\n");
    }

    private void renderFooter() {
        StringBuilder sb = new StringBuilder();
        sb.append(format("\t\t\t\t\t\t\t\t\t\t\t[Pozycja: %d/%d]\n", step, traders.size() + directors.size()));
        sb.append("\t[N] - następny\n");
        sb.append("\t[Q] - powrót\n");
        sb.append("Wybór>");
        System.out.println(sb.toString());
    }

    private void getInitialData() {
        directors = directorDao.getAll();
        traders = traderDao.getAll();
    }
}
