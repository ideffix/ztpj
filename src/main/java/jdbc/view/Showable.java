package jdbc.view;

public interface Showable {
    void show();
}
