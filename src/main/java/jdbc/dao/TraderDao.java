package jdbc.dao;

import jdbc.connection.ConnectionFactory;
import jdbc.mapper.TraderMapper;
import jdbc.model.Trader;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TraderDao implements Dao<Trader>  {

    @Override
    public Optional<Trader> get(int id) {
        try (Connection conn = ConnectionFactory.getConnection()) {
            try (
                    PreparedStatement stmt = createGetPreparedStatement(conn, id);
                    ResultSet rs = stmt.executeQuery()
            ) {
                conn.commit();
                if (rs.next()) {
                    return Optional.of(TraderMapper.map(rs));
                }
            } catch (SQLException e) {
                conn.rollback();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private PreparedStatement createGetPreparedStatement(Connection conn, int id) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement("SELECT * FROM traders WHERE id=?");
        stmt.setInt(1, id);
        return stmt;
    }

    @Override
    public List<Trader> getAll() {
        List<Trader> traders = new ArrayList<>();
        try (Connection conn = ConnectionFactory.getConnection()) {
            try (Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM traders")) {
                conn.commit();
                while (rs.next()) {
                    traders.add(TraderMapper.map(rs));
                }
            } catch (SQLException e) {
                conn.rollback();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return traders;
    }

    @Override
    public void save(Trader trader) {
        try (Connection conn = ConnectionFactory.getConnection()) {
            try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO traders" +
                    "(pesel, name, surname, position, salary, phoneNumber, comissionRate, comissionRateLimitPerMonth) VALUES " +
                    "(?,?,?,?,?,?,?,?)")) {
                stmt.setString(1, trader.getPesel());
                stmt.setString(2, trader.getName());
                stmt.setString(3, trader.getSurname());
                stmt.setString(4, trader.getPosition());
                stmt.setBigDecimal(5, trader.getSalary());
                stmt.setString(6, trader.getPhoneNumber());
                stmt.setBigDecimal(7, trader.getComissionRate());
                stmt.setBigDecimal(8, trader.getComissionRateLimitPerMonth());
                stmt.executeUpdate();
                conn.commit();
            } catch (SQLException e) {
                conn.rollback();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        try (Connection conn = ConnectionFactory.getConnection()) {
            try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM traders WHERE id=?")) {
                stmt.setInt(1, id);
                stmt.executeUpdate();
                conn.commit();
            } catch (SQLException e) {
                conn.rollback();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Optional<Trader> getByPesel(String pesel) {
        try (Connection conn = ConnectionFactory.getConnection()) {
            try (
                    PreparedStatement stmt = createPeselGetPreparedStatement(conn, pesel);
                    ResultSet rs = stmt.executeQuery()
            ) {
                conn.commit();
                if (rs.next()) {
                    return Optional.of(TraderMapper.map(rs));
                }
            } catch (SQLException e) {
                conn.rollback();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }


    private PreparedStatement createPeselGetPreparedStatement(Connection conn, String pesel) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement("SELECT * FROM traders WHERE pesel=?");
        stmt.setString(1, pesel);
        return stmt;
    }
}
