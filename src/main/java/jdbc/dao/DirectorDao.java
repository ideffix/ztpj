package jdbc.dao;

import jdbc.connection.ConnectionFactory;
import jdbc.mapper.DirectorMapper;
import jdbc.model.Director;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DirectorDao implements Dao<Director> {

    @Override
    public Optional<Director> get(int id) {
        try (Connection conn = ConnectionFactory.getConnection()) {
            try (
                PreparedStatement stmt = createGetPreparedStatement(conn, id);
                ResultSet rs = stmt.executeQuery()
            ) {
                conn.commit();
                if (rs.next()) {
                    return Optional.of(DirectorMapper.map(rs));
                } else {
                    return Optional.empty();
                }
            } catch (SQLException e) {
                conn.rollback();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private PreparedStatement createGetPreparedStatement(Connection conn, int id) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement("SELECT * FROM directors WHERE id=?");
        stmt.setInt(1, id);
        return stmt;
    }

    @Override
    public List<Director> getAll() {
        List<Director> directors = new ArrayList<>();
        try (Connection conn = ConnectionFactory.getConnection()) {
            try (Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM directors")) {
                conn.commit();
                while (rs.next()) {
                    directors.add(DirectorMapper.map(rs));
                }
            } catch (SQLException e) {
                conn.rollback();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return directors;
    }

    @Override
    public void save(Director director) {
        try (Connection conn = ConnectionFactory.getConnection()) {
            try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO directors" +
                    "(pesel, name, surname, position, salary, phoneNumber, businessAllowance, cardNumber, costLimitPerMonth) VALUES " +
                    "(?,?,?,?,?,?,?,?,?)")) {
                stmt.setString(1, director.getPesel());
                stmt.setString(2, director.getName());
                stmt.setString(3, director.getSurname());
                stmt.setString(4, director.getPosition());
                stmt.setBigDecimal(5, director.getSalary());
                stmt.setString(6, director.getPhoneNumber());
                stmt.setBigDecimal(7, director.getBusinessAllowance());
                stmt.setString(8, director.getCardNumber());
                stmt.setBigDecimal(9, director.getCostLimitPerMonth());
                stmt.executeUpdate();
                conn.commit();
            } catch (SQLException e) {
                conn.rollback();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        try (Connection conn = ConnectionFactory.getConnection()) {
            try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM directors WHERE id=?")) {
                stmt.setInt(1, id);
                stmt.executeUpdate();
                conn.commit();
            } catch (SQLException e) {
                conn.rollback();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Optional<Director> getByPesel(String pesel) {
        try (Connection conn = ConnectionFactory.getConnection()) {
            try (
                    PreparedStatement stmt = createPeselGetPreparedStatement(conn, pesel);
                    ResultSet rs = stmt.executeQuery()
            ) {
                conn.commit();
                if (rs.next()) {
                    return Optional.of(DirectorMapper.map(rs));
                }
            } catch (SQLException e) {
                conn.rollback();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }


    private PreparedStatement createPeselGetPreparedStatement(Connection conn, String pesel) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement("SELECT * FROM directors WHERE pesel=?");
        stmt.setString(1, pesel);
        return stmt;
    }
}
