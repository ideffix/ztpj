package jdbc.utils;

import jdbc.model.Director;
import jdbc.model.Employee;
import jdbc.model.Trader;
import jdbc.provider.UserInputProvider;

import static java.lang.String.format;

public class PrintUtils {

    public static void renderTraderView(Trader trader) {
        renderEmployeeView(trader);
        StringBuilder sb = new StringBuilder();
        sb.append(format("\t\tProwizja\t\t\t\t\t:\t\t%s\n", trader.getComissionRate()));
        sb.append(format("\t\tLimit prowizji/miesiąc (zł)\t:\t\t%s\n", trader.getComissionRateLimitPerMonth()));
        System.out.println(sb.toString());
    }

    public static void renderDirectorView(Director director) {
        renderEmployeeView(director);
        StringBuilder sb = new StringBuilder();
        sb.append(format("\t\tDodatek służbowy (zł)\t\t:\t\t%s\n", director.getBusinessAllowance()));
        sb.append(format("\t\tKarta służbowa numer\t\t:\t\t%s\n", director.getCardNumber()));
        sb.append(format("\t\tLimit kosztów/miesiąc (zł)\t:\t\t%s\n", director.getCostLimitPerMonth()));
        System.out.println(sb.toString());
    }

    private static void renderEmployeeView(Employee employee) {
        StringBuilder sb = new StringBuilder();
        sb.append(format("\t\tIdentyfikator PESEL\t\t\t:\t\t%s\n", employee.getPesel()));
        sb.append(format("\t\tImię\t\t\t\t\t\t:\t\t%s\n", employee.getName()));
        sb.append(format("\t\tNazwisko\t\t\t\t\t:\t\t%s\n", employee.getSurname()));
        sb.append(format("\t\tStanowisko\t\t\t\t\t:\t\t%s\n", employee.getPosition()));
        sb.append(format("\t\tWynagrodzenie (zł)\t\t\t:\t\t%s\n", employee.getSalary()));
        sb.append(format("\t\tNumer telefonu\t\t\t\t:\t\t%s", employee.getPhoneNumber()));
        System.out.println(sb.toString());
    }

    public static String printAndGetUserInput(String text) {
        System.out.println("\t" + text + "\n");
        return UserInputProvider.getUserInput();
    }

    public static String printAndHiddenGetUserInput(String text) {
        System.out.println("\t" + text + "\n");
        return UserInputProvider.getHiddenUserInput();
    }
}
