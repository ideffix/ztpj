package jdbc.utils;

public interface Action {
    void invoke();
}
