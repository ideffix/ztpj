package jdbc.mapper;

import jdbc.model.Director;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DirectorMapper {

    public static Director map(ResultSet rs) throws SQLException {
        Director director = new Director();
        EmployeeMapper.mapExisting(rs, director);
        director.setBusinessAllowance(rs.getBigDecimal("businessAllowance"));
        director.setCardNumber(rs.getString("cardNumber"));
        director.setCostLimitPerMonth(rs.getBigDecimal("costLimitPerMonth"));
        return director;
    }
}
