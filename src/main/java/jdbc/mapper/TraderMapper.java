package jdbc.mapper;

import jdbc.model.Trader;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TraderMapper {

    public static Trader map(ResultSet rs) throws SQLException {
        Trader trader = new Trader();
        EmployeeMapper.mapExisting(rs, trader);
        trader.setComissionRate(rs.getBigDecimal("comissionRate"));
        trader.setComissionRateLimitPerMonth(rs.getBigDecimal("comissionRateLimitPerMonth"));
        return trader;
    }
}
