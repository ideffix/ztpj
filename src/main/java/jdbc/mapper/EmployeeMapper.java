package jdbc.mapper;

import jdbc.model.Employee;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeMapper {

    public static void mapExisting(ResultSet rs, Employee employee) throws SQLException {
                employee.setId(rs.getInt("id"));
                employee.setName(rs.getString("name"));
                employee.setSurname(rs.getString("surname"));
                employee.setPesel(rs.getString("pesel"));
                employee.setPosition(rs.getString("position"));
                employee.setPhoneNumber(rs.getString("phoneNumber"));
                employee.setSalary(rs.getBigDecimal("salary"));
    }
}
