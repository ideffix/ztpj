import jdbc.view.MainView;
import networking.NetworkConfig;
import networking.server.EchoServer;
import rmi.server.RMIServer;

public class App {

    public static void main(String... args) {
        EchoServer.createDaemon(NetworkConfig.PORT);
        RMIServer.start();
        MainView mainView = new MainView();
        mainView.show();
    }
}
