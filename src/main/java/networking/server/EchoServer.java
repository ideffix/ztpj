package networking.server;

import lombok.AllArgsConstructor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

@AllArgsConstructor
public class EchoServer implements Runnable {

    private int port;

    public static void createDaemon(int port) {
        Thread thread = new Thread(new EchoServer(port));
        thread.setDaemon(true);
        thread.start();
    }

    @Override
    public void run() {
        try {
            int i= 1;
            ServerSocket s = new ServerSocket(port);
            while (true) {
                Socket incoming = s.accept();
                System.out.println ("Spawning " + i);
                Runnable r = new ClientHandler(incoming);
                Thread t = new Thread(r);
                t.start();
                i++;
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
