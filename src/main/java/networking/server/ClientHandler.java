package networking.server;

import lombok.AllArgsConstructor;
import networking.datasource.EmployeeDataSource;
import rmi.validator.Validator;
import rmi.validator.impl.RemoteValidatorImpl;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

@AllArgsConstructor
public class ClientHandler implements Runnable {
    private Socket socket;
    private final Validator validator = new RemoteValidatorImpl();

    @Override
    public void run() {
        try {
            try (InputStream inStream = socket.getInputStream();
                 OutputStream outStream = socket.getOutputStream();
                 Scanner in = new Scanner(inStream);
                 PrintWriter out = new PrintWriter(outStream, true);
                 ObjectOutputStream oos = new ObjectOutputStream(outStream)) {
                out.println("Podaj token aby dostac dane");
                if (in.hasNextLine()) {
                    String token = in.nextLine();
                    if (validator.hasAccess(token)) {
                        sendData(oos);
                        out.println("BYE");
                    }
                }
            } finally {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendData(ObjectOutputStream oos) throws IOException {
        oos.writeObject(EmployeeDataSource.getEmployees());
    }
}
