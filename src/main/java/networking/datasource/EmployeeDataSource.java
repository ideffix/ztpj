package networking.datasource;

import jdbc.model.Director;
import jdbc.model.Employee;
import jdbc.model.Trader;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class EmployeeDataSource {

    private static List<Employee> employees = Arrays.asList(
            new Director(
                    "94050510194",
                    "Bartosz",
                    "Kopciuch",
                    "Szef",
                    BigDecimal.valueOf(15_000),
                    "723503639",
                    BigDecimal.valueOf(123),
                    "321654876",
                    BigDecimal.valueOf(100)
            ),
            new Director(
                    "9010109458531",
                    "Marek",
                    "Kanarek",
                    "Zastepca",
                    BigDecimal.valueOf(10_000),
                    "502222123",
                    BigDecimal.valueOf(120),
                    "123456789",
                    BigDecimal.valueOf(50)
            ),
            new Trader(
                    "8502037710585",
                    "Marian",
                    "Kowalski",
                    "Sprzedawczyk",
                    BigDecimal.valueOf(5_000),
                    "666222333",
                    BigDecimal.valueOf(200),
                    BigDecimal.valueOf(170)
            )
    );

    public static List<Employee> getEmployees() {
        return employees;
    }
}
