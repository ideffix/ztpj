package networking.client;

import jdbc.model.Employee;
import lombok.AllArgsConstructor;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@AllArgsConstructor
public class Client {
    private String address;
    private int port;

    public List<Employee> getEmployees(String token) {
        try (Socket socket = new Socket(address, port);
             InputStream inStream = socket.getInputStream();
             OutputStream outStream = socket.getOutputStream();
             Scanner in = new Scanner(inStream);
             PrintWriter out = new PrintWriter(outStream, true);
             ObjectInputStream ois = new ObjectInputStream(inStream)) {
            System.out.println(in.nextLine());
            out.println(token);
            return (List<Employee>) ois.readObject();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

}
