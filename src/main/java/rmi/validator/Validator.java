package rmi.validator;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Validator extends Remote {

    String authenticate(String username, String password) throws RemoteException;

    boolean hasAccess(String token) throws RemoteException;
}
