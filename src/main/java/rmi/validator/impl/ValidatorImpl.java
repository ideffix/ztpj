package rmi.validator.impl;

import rmi.exception.ValidationException;
import rmi.model.Token;
import rmi.token.TokenGenerator;
import rmi.validator.Validator;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidatorImpl extends UnicastRemoteObject implements Validator {

    private static Map<String, String> users = new HashMap<>();

    private List<Token> validTokens = new ArrayList<>();

    static {
        users.put("kb29466", "password1");
    }

    public ValidatorImpl() throws RemoteException {
    }

    @Override
    public String authenticate(String username, String password) throws RemoteException {
        checkTokensValidity();
        if (users.containsKey(username) &&
            users.get(username).equals(password)) {
            Token token = TokenGenerator.generateToken();
            validTokens.add(token);
            return token.getValue();
        }
        throw new ValidationException();
    }

    @Override
    public boolean hasAccess(String token) throws RemoteException {
        boolean hasAccess = validTokens.stream()
                .anyMatch(t -> t.getValue().equals(token) && isTokenValid(t));
        if (hasAccess) {
            validTokens.removeIf(t -> t.getValue().equals(token));
        }
        return hasAccess;
    }

    private void checkTokensValidity() {
        validTokens.removeIf(this::isTokenNotValid);
    }

    private boolean isTokenNotValid(Token token) {
        return !isTokenValid(token);
    }

    private boolean isTokenValid(Token token) {
        LocalDateTime now = LocalDateTime.now();
        return ChronoUnit.MINUTES.between(token.getDate(), now) <= 2;
    }
}
