package rmi.validator.impl;

import rmi.validator.Validator;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class RemoteValidatorImpl implements Validator {

    private Validator validator;

    public RemoteValidatorImpl() {
        try {
            this.validator = (Validator) Naming.lookup("validator");
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String authenticate(String username, String password) throws RemoteException {
        return validator.authenticate(username, password);
    }

    @Override
    public boolean hasAccess(String token) throws RemoteException {
        return validator.hasAccess(token);
    }
}
