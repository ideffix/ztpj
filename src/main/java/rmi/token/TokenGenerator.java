package rmi.token;

import rmi.model.Token;

import java.time.LocalDateTime;
import java.util.UUID;

public class TokenGenerator {

    public static Token generateToken() {
        return Token.builder()
                .value(UUID.randomUUID().toString())
                .date(LocalDateTime.now())
                .build();
    }
}
