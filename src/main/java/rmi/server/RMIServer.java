package rmi.server;

import rmi.validator.impl.ValidatorImpl;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class RMIServer {

    public static void start() {
        try {
            ValidatorImpl validator = new ValidatorImpl();
            Naming.rebind("validator", validator);
            System.out.println("RMIServer started");
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
